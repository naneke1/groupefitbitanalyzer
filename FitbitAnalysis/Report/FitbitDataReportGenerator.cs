﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using FitbitAnalysis.Model;

namespace FitbitAnalysis.Report
{
    /// <summary>
    ///     Gives a report of all fitbit data.
    /// </summary>
    public class FitbitDataReportGenerator
    {
        #region Data members

        private const int Months = 12;

        private readonly FitbitDataLibrary fitbitDataLibrary;

        private readonly StringBuilder stringBuilder;

        private List<FitbitData> dataCollection;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the threshold.
        /// </summary>
        /// <value>
        ///     The threshold.
        /// </value>
        public int Threshold { get; set; }

        /// <summary>
        ///     Gets or sets the steps.
        /// </summary>
        /// <value>
        ///     The steps.
        /// </value>
        public int Steps { get; set; }

        /// <summary>
        ///     Gets or sets the category.
        /// </summary>
        /// <value>
        ///     The category.
        /// </value>
        public int Category { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="FitbitDataReportGenerator" /> class.
        /// </summary>
        public FitbitDataReportGenerator(FitbitDataLibrary library)
        {
            this.fitbitDataLibrary = library;
            this.stringBuilder = new StringBuilder();
            this.Threshold = 10000;
            this.Steps = 2500;
            this.Category = 6;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Displays a report of fitbit data.
        /// </summary>
        /// <returns></returns>
        public string DisplayFitbitDataReport()
        {
            this.generateYearlyStepBreakdown();
            return this.stringBuilder.ToString();
        }

        /// <summary>
        ///     Clears this instance.
        /// </summary>
        public void Clear()
        {
            this.fitbitDataLibrary.Clear();
        }

        /// <summary>
        ///     Adds the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void Add(FitbitData data)
        {
            this.fitbitDataLibrary.Add(data);
        }

        private void generateYearlyStepBreakdown()
        {
            var years = new List<int>();

            years.AddRange(this.hasMultipleYearsOfData());

            foreach (var year in years)
            {
                this.dataCollection = new List<FitbitData>();

                this.groupFitbitDataByYear(year);

                this.getMaxStepsAllYear(year);

                this.getMinStepsAllYear(year);

                this.getAverageStepsAllYear();

                this.generateMaximunActivityMinutesAllYear(year);

                var stepcount = this.GetDaysWithStepsMoreThan(this.Threshold);
                this.stringBuilder.AppendLine("# of days with steps more than " +
                                              string.Format("{0:n0}", this.Threshold) + " is " + stepcount);

                this.GetDaysWithStepsBetween(this.Steps, this.Category);

                this.displayDaysInYearWithNoData(year);

                this.stringBuilder.AppendLine("\n");

                this.generateMonthlyStepBreakdown(year);
            }
        }

        private void groupFitbitDataByYear(int year)
        {
            foreach (var fitbitdata in this.fitbitDataLibrary)
            {
                if (year.Equals(fitbitdata.Date.Year))
                {
                    this.dataCollection.Add(fitbitdata);
                }
            }
        }

        private void generateMonthlyStepBreakdown(int year)
        {
            IDictionary<int, List<FitbitData>> months = new Dictionary<int, List<FitbitData>>();

            for (var i = 1; i <= Months; i++)
            {
                months.Add(i, new List<FitbitData>());
            }

            foreach (var month in months)
            {
                this.addFitbitDataToMonth(month);

                this.displayOutputForAllMonths(year, month);
            }
        }

        private void addFitbitDataToMonth(KeyValuePair<int, List<FitbitData>> month)
        {
            var allData = this.fitbitDataLibrary.Where(data => data.Date.Month.Equals(month.Key));
            month.Value.AddRange(allData);
        }

        private void displayOutputForAllMonths(int year, KeyValuePair<int, List<FitbitData>> month)
        {
            if (month.Value.Any(fitBit => fitBit.Date.Month == month.Key && fitBit.Date.Year == year))
            {
                this.displayOutputForMonthsWithData(year, month);
            }
            else
            {
                this.displayOutputForMonthsWithNoData(year, month);
            }
        }

        private void displayOutputForMonthsWithNoData(int year, KeyValuePair<int, List<FitbitData>> month)
        {
            var index = month.Key;
            var monthName = new DateTimeFormatInfo().GetMonthName(index);
            this.stringBuilder.AppendLine(monthName + " " + year + " (" +
                                          this.fitbitDataLibrary.CountdaysInMonth(year, month.Key) + " days of data)");
            this.stringBuilder.AppendLine("\n");
        }

        private void displayOutputForMonthsWithData(int year, KeyValuePair<int, List<FitbitData>> month)
        {
            var index = month.Key;
            var monthName = new DateTimeFormatInfo().GetMonthName(index);
            this.stringBuilder.AppendLine(monthName + " " + year + " (" +
                                          this.fitbitDataLibrary.CountdaysInMonth(year, month.Key) + " days of data)");

            var maxSteps = this.fitbitDataLibrary.RetrieveStepsMaxForMonth(year, month.Key);
            this.stringBuilder.AppendLine("Most steps: " + string.Format("{0:n0}", maxSteps));

            var minSteps = this.fitbitDataLibrary.RetrieveMinStepsForMonth(year, month.Key);
            this.stringBuilder.AppendLine("Fewest steps: " + string.Format("{0:n0}", minSteps));

            var averageSteps = this.fitbitDataLibrary.CalculateAverageForMonth(year, month.Key);
            this.stringBuilder.AppendLine("Average # of steps: " + string.Format("{0:n}", averageSteps));

            this.generateAverageActivityMinutesForMonthYear(year, month.Key);
            this.stringBuilder.AppendLine("\n");
        }

        private void displayDaysInYearWithNoData(int year)
        {
            var dataCount = this.fitbitDataLibrary.CountDaysInYearWithNoData(year);
            this.stringBuilder.AppendLine("# of days with no data: " + dataCount);
        }

        /// <summary>
        ///     Dayses the with steps more than.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public int GetDaysWithStepsMoreThan(int value)
        {
            this.Threshold = value;
            var stepcount = this.fitbitDataLibrary.Count(data => data.Steps > value);
            return stepcount;
        }

        /// <summary>
        ///     Dayses the with steps between.
        /// </summary>
        /// <param name="steps">The steps.</param>
        /// <param name="category">The category.</param>
        public void GetDaysWithStepsBetween(int steps, int category)
        {
            this.Steps = steps;
            this.Category = category;
            var lowerbound = 0;
            var upperbound = steps;
            var count = 1;

            if (this.stringBuilder.Length == 0)
            {
                return;
            }

            while (count != category)
            {
                var daysBetweenLowerAndUpperBound =
                    this.fitbitDataLibrary.Count(data => data.Steps >= lowerbound && data.Steps <= upperbound);
                this.stringBuilder.AppendLine("Days with steps between " + lowerbound + " and " + upperbound + " is " +
                                              daysBetweenLowerAndUpperBound);
                lowerbound = lowerbound + steps + 1;
                upperbound = upperbound + steps;
                count++;
            }
            var daysGreaterThanUpperBound = this.fitbitDataLibrary.Count(data => data.Steps > upperbound);
            this.stringBuilder.AppendLine("Days greater than " + (upperbound - steps) + " is " +
                                          daysGreaterThanUpperBound);
        }

        /// <summary>
        ///     Gets the fitbit data in report.
        /// </summary>
        /// <returns></returns>
        public string GetFitbitDataInReport(FitbitDataLibrary library)
        {
            var builder = new StringBuilder();
            foreach (var data in library)
            {
                builder.AppendLine(data.Date + "," + data.CaloriesBurned + "," + data.Steps + "," +
                                   data.Distance + "," + data.Floors + "," + data.ActivityCalories + "," +
                                   data.ActiveMinutes);
            }
            return builder.ToString();
        }

        private void getAverageStepsAllYear()
        {
            var averageSteps = this.dataCollection.Average(fitbitData => fitbitData.Steps);
            this.stringBuilder.AppendLine($"Average # of steps taken all year: {Math.Round(averageSteps, 2)}");
        }

        private void getMinStepsAllYear(int year)
        {
            var minSteps = this.dataCollection.Min(fitbitData => fitbitData.Steps);
            var date = this.dataCollection.Find(data => data.Steps == minSteps).Date;
            this.stringBuilder.AppendLine($"Least steps taken in {year}" +
                                          $" {string.Format("{0:n0}", minSteps)} and occured on {date:MM/dd/yyyy}");
        }

        private string convertToHourAndMinutes(double activityMinutes)
        {
            var span = TimeSpan.FromMinutes(activityMinutes);
            var display = span.ToString("hh\\:mm");
            return display;
        }

        private void generateAverageActivityMinutesForMonthYear(int year, int month)
        {
            var average = this.fitbitDataLibrary.RetrieveAverageActivityMinutesAInMonth(year, month);
            this.stringBuilder.AppendLine(
                $"Average activity minutes for month: {this.convertToHourAndMinutes(Math.Round(average, 2))}");
        }

        private void generateMaximunActivityMinutesAllYear(int year)
        {
            var fitBit = this.fitbitDataLibrary.RetrieveHighestActivityMinutesAllYear(year);
            this.stringBuilder.AppendLine(
                $"Most Activity Minutes in {year}: {this.convertToHourAndMinutes(fitBit.ActiveMinutes)} and occured on {fitBit.Date:MM/dd/yyyy}");
        }

        private void getMaxStepsAllYear(int year)
        {
            var maxSteps = this.dataCollection.Max(fitbitData => fitbitData.Steps);
            var date = this.dataCollection.Find(data => data.Steps == maxSteps).Date;
            this.stringBuilder.AppendLine($"Most steps taken in {year}" +
                                          $" {string.Format("{0:n0}", maxSteps)} and occured on {date:MM/dd/yyyy}");
        }

        private List<int> hasMultipleYearsOfData()
        {
            var years = new List<int>();

            foreach (var data in this.fitbitDataLibrary)
            {
                if (!years.Contains(data.Date.Year))
                {
                    years.Add(data.Date.Year);
                }
            }
            return years;
        }

        #endregion
    }
}