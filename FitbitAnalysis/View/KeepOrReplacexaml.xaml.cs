﻿using Windows.UI.Xaml;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace FitbitAnalysis.View
{
    /// <summary>
    /// </summary>
    public enum Result
    {
        /// <summary>
        ///     The keep
        /// </summary>
        Keep,

        /// <summary>
        ///     The keep all
        /// </summary>
        KeepAll,

        /// <summary>
        ///     The replace all
        /// </summary>
        ReplaceAll,

        /// <summary>
        ///     The Replace
        /// </summary>
        Replace,

        /// <summary>
        ///     The nothing
        /// </summary>
        Nothing
    }

    /// <summary>
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.ContentDialog" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    public sealed partial class KeepOrReplacexaml
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the click.
        /// </summary>
        /// <value>
        ///     The click.
        /// </value>
        public Result Click { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeepOrReplacexaml" /> class.
        /// </summary>
        public KeepOrReplacexaml()
        {
            this.InitializeComponent();
            this.Click = Result.Nothing;
        }

        #endregion

        #region Methods

        private void KeepButton_Click(object sender, RoutedEventArgs e)
        {
            this.Click = Result.Keep;
            this.keepOrReplaceDialog.Hide();
        }

        private void KeepAllButton_Click(object sender, RoutedEventArgs e)
        {
            this.Click = Result.KeepAll;
            this.keepOrReplaceDialog.Hide();
        }

        private void ReplaceAllButton_Click(object sender, RoutedEventArgs e)
        {
            this.Click = Result.ReplaceAll;
            this.keepOrReplaceDialog.Hide();
        }

        private void ReplaceButton_Click(object sender, RoutedEventArgs e)
        {
            this.Click = Result.Replace;
            this.keepOrReplaceDialog.Hide();
        }

        #endregion
    }
}