﻿using System;
using Windows.UI.Xaml;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace FitbitAnalysis.View
{
    /// <summary>
    ///     An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddFitbitDialog
    {
        #region Types and Delegates

        /// <summary>
        /// </summary>
        public enum Result
        {
            /// <summary>
            ///     The add
            /// </summary>
            Add,

            /// <summary>
            ///     The cancel
            /// </summary>
            Cancel
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the date.
        /// </summary>
        /// <value>
        ///     The date.
        /// </value>
        public DateTime Date { get; set; }

        /// <summary>
        ///     Gets or sets the calories burned.
        /// </summary>
        /// <value>
        ///     The calories burned.
        /// </value>
        public int CaloriesBurned { get; set; }

        /// <summary>
        ///     Gets or sets the steps.
        /// </summary>
        /// <value>
        ///     The steps.
        /// </value>
        public int Steps { get; set; }

        /// <summary>
        ///     Gets or sets the distance.
        /// </summary>
        /// <value>
        ///     The distance.
        /// </value>
        public double Distance { get; set; }

        /// <summary>
        ///     Gets or sets the floors.
        /// </summary>
        /// <value>
        ///     The floors.
        /// </value>
        public int Floors { get; set; }

        /// <summary>
        ///     Gets or sets the activity calories.
        /// </summary>
        /// <value>
        ///     The activity calories.
        /// </value>
        public int ActivityCalories { get; set; }

        /// <summary>
        ///     Gets or sets the activity minutes.
        /// </summary>
        /// <value>
        ///     The activity minutes.
        /// </value>
        public int ActivityMinutes { get; set; }

        /// <summary>
        ///     Gets or sets the click.
        /// </summary>
        /// <value>
        ///     The click.
        /// </value>
        public Result Click { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AddFitbitDialog" /> class.
        /// </summary>
        public AddFitbitDialog()
        {
            this.InitializeComponent();
            this.Click = Result.Cancel;
        }

        #endregion

        #region Methods

        private void addButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.Click = Result.Add;
            this.Date = this.datepicker.Date.Date;

            this.CaloriesBurned = Convert.ToInt32(this.caloriesBurnedTextBox.Text.Replace("_", ""));
            this.Steps = Convert.ToInt32(this.stepsTextBox.Text.Replace("_", ""));
            this.Distance = Convert.ToDouble(this.distanceTextBox.Text.Replace("_", ""));
            this.Floors = Convert.ToInt32(this.floorsTextBox.Text.Replace("_", ""));
            this.ActivityCalories = Convert.ToInt32(this.activityCaloriesTextBox.Text.Replace("_", ""));
            this.ActivityMinutes = Convert.ToInt32(this.activityMinutesTextBox.Text.Replace("_", ""));
            this.addFitbitDialog.Hide();
        }

        private void cancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.Click = Result.Cancel;
            this.addFitbitDialog.Hide();
        }

        #endregion
    }
}