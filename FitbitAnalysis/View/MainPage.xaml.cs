﻿using System;
using Windows.Foundation;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using FitbitAnalysis.Model;
using FitbitAnalysis.Report;
using FitbitAnalysis.ViewModel;

namespace FitbitAnalysis.View
{
    /// <summary>
    ///     An main page that will allow the user to load a CSV file of Fitbit data.
    /// </summary>
    public sealed partial class MainPage
    {
        #region Data members

        /// <summary>
        ///     The application height
        /// </summary>
        public const int ApplicationHeight = 600;

        /// <summary>
        ///     The application width
        /// </summary>
        public const int ApplicationWidth = 1250;

        private readonly FitbitDataLibrary dataLibrary;

        private FitbitDataReportGenerator dataReport;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainPage" /> class.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();

            ApplicationView.PreferredLaunchViewSize = new Size {Width = ApplicationWidth, Height = ApplicationHeight};
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(ApplicationWidth, ApplicationHeight));

            this.dataLibrary = FitbitDataDetailViewModel.Library;
        }

        #endregion

        #region Methods

        private void clearButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.outputTextBox.Text = string.Empty;
        }

        private void GoBack_Button_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }

        private void stepsThreshold_TextChanged(object sender, TextChangedEventArgs e)
        {
            var categoryTextBoxText = this.categoryTextBox.Text.Replace("_", string.Empty);
            var stepsextBoxText = this.stepsTextBox.Text.Replace("_", string.Empty);
            if (categoryTextBoxText != string.Empty && stepsextBoxText != string.Empty)
            {
                var steps = Convert.ToInt32(stepsextBoxText);
                var category = Convert.ToInt32(categoryTextBoxText);
                this.dataReport = new FitbitDataReportGenerator(this.dataLibrary);
                this.dataReport.GetDaysWithStepsBetween(steps, category);
                this.outputTextBox.Text = this.dataReport.DisplayFitbitDataReport();
            }
        }

        private void StepsMoreThanTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var textBoxText = this.stepsMoreThanTextBox.Text.Replace("_", string.Empty);
            if (textBoxText != string.Empty)
            {
                var steps = Convert.ToInt32(textBoxText);
                this.dataReport = new FitbitDataReportGenerator(this.dataLibrary);
                this.dataReport.GetDaysWithStepsMoreThan(steps);
                this.outputTextBox.Text = this.dataReport.DisplayFitbitDataReport();
            }
        }

        #endregion
    }
}