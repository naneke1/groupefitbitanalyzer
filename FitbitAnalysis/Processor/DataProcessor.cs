﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;
using FitbitAnalysis.Model;
using FitbitAnalysis.View;

namespace FitbitAnalysis.Processor
{
    /// <summary>
    ///     This class processes a file that has been inputed in the application.
    /// </summary>
    public class DataProcessor
    {
        #region Data members

        /// <summary>
        ///     The header of the file
        /// </summary>
        private const string Header = "Date";

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the library.
        /// </summary>
        /// <value>
        ///     The library.
        /// </value>
        public FitbitDataLibrary Library { get; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataProcessor" /> class.
        /// </summary>
        /// <param name="library">The library.</param>
        public DataProcessor(FitbitDataLibrary library)
        {
            this.Library = library;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Processes the specified file.
        /// </summary>
        /// <param name="file">The file.</param>
        public async Task ProcessDataWhenEmpty(StorageFile file)
        {
            var addingList = await addAllFitBitToList(file);

            this.Library.DataList.AddRange(addingList);
        }

        private static async Task<FitbitDataLibrary> addAllFitBitToList(StorageFile file)
        {
            var lines = await FileIO.ReadLinesAsync(file);

            if (lines.ElementAt(0).Contains(Header))
            {
                lines.RemoveAt(0);
            }

            var addingList = new FitbitDataLibrary();
            foreach (var aline in lines)
            {
                var delimeter = new[] {','};
                var splitLine = aline.Split(delimeter);

                var date = Convert.ToDateTime(splitLine[0]);
                var caloriesBurned = Convert.ToInt32(splitLine[1]);
                var steps = Convert.ToInt32(splitLine[2]);
                var distance = Convert.ToDouble(splitLine[3]);
                var floors = Convert.ToInt32(splitLine[4]);
                var activityCalories = Convert.ToInt32(splitLine[5]);
                var activityMinutes = Convert.ToInt32(splitLine[6]);

                var fitbit = new FitbitData {
                    Date = date,
                    CaloriesBurned = caloriesBurned,
                    Steps = steps,
                    Distance = distance,
                    Floors = floors,
                    ActivityCalories = activityCalories,
                    ActiveMinutes = activityMinutes
                };

                addingList.Add(fitbit);
            }
            return addingList;
        }

        /// <summary>
        ///     Merges the specified file.
        /// </summary>
        /// <param name="file">The file.</param>
        public async Task Merge(StorageFile file)
        {
            var keepreplace = new KeepOrReplacexaml();
            var addList = await addAllFitBitToList(file);
            var copyList = addList.ToList();

            var exitLoop = false;
            foreach (var singleFitBit in copyList)
            {
                if (copyList.Any(x => x.Date == singleFitBit.Date))
                {
                    await keepreplace.ShowAsync();

                    switch (keepreplace.Click)
                    {
                        case Result.Keep:
                            break;
                        case Result.KeepAll:
                            exitLoop = true;
                            break;
                        case Result.Replace:
                            var fitbitDatas = this.Library.DataList.FirstOrDefault(k => k.Date == singleFitBit.Date);
                            var index = this.Library.DataList.IndexOf(fitbitDatas);
                            this.Library[index] = singleFitBit;
                            addList.Remove(singleFitBit);
                            break;
                        case Result.ReplaceAll:
                            exitLoop = true;
                            break;
                    }
                }
                if (exitLoop)
                {
                    break;
                }
            }

            if (keepreplace.Click == Result.Replace)
            {
                this.Library.DataList.AddRange(addList);
            }
            if (keepreplace.Click == Result.ReplaceAll)
            {
                foreach (var fitbitData in copyList)
                {
                    if (this.Library.Any(y => y.Date == fitbitData.Date))
                    {
                        var fitbitDatas = this.Library.DataList.FirstOrDefault(k => k.Date == fitbitData.Date);
                        var index = this.Library.DataList.IndexOf(fitbitDatas);
                        this.Library[index] = fitbitData;
                        addList.Remove(fitbitData);
                    }
                }
                this.Library.DataList.AddRange(addList);
            }
        }

        #endregion
    }
}