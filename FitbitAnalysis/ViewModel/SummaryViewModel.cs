﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using FitbitAnalysis.Model;
using FitbitAnalysis.Report;

namespace FitbitAnalysis.ViewModel
{
    /// <summary>
    /// </summary>
    public class SummaryViewModel : INotifyPropertyChanged
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the library.
        /// </summary>
        /// <value>
        ///     The library.
        /// </value>
        public FitbitDataLibrary Library { get; set; }

        /// <summary>
        ///     Gets or sets the summary.
        /// </summary>
        /// <value>
        ///     The summary.
        /// </value>
        public string Summary { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="SummaryViewModel" /> class.
        /// </summary>
        public SummaryViewModel()
        {
            this.Library = new FitbitDataLibrary();
            this.Library.DataList.AddRange(FitbitDataDetailViewModel.Library);
            var reportGenerator = new FitbitDataReportGenerator(this.Library);
            this.Summary = reportGenerator.DisplayFitbitDataReport();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}