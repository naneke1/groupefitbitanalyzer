﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml.Controls;
using FitbitAnalysis.Extensions;
using FitbitAnalysis.Model;
using FitbitAnalysis.Processor;
using FitbitAnalysis.Report;
using FitbitAnalysis.Utility;
using FitbitAnalysis.View;

namespace FitbitAnalysis.ViewModel
{
    /// <summary>
    ///     This class represents a user interface for the list detail view of the application
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public class FitbitDataDetailViewModel : INotifyPropertyChanged
    {
        #region Data members

        private FitbitDataLibrary dataLibrary;

        private readonly FitbitDataReportGenerator reportGenerator;

        /// <summary>
        ///     The fitbit datas
        /// </summary>
        private ObservableCollection<FitbitData> fitbitDatas;

        private ObservableCollection<int> years;

        private int selectedYear;

        private FitbitData selectedFitbitData;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the select year command.
        /// </summary>
        /// <value>
        ///     The select year command.
        /// </value>
        public RelayCommand SelectYearCommand { get; set; }

        /// <summary>
        ///     Gets or sets the save as XML command.
        /// </summary>
        /// <value>
        ///     The save as XML command.
        /// </value>
        public RelayCommand SaveAsXmlCommand { get; set; }

        /// <summary>
        ///     Gets or sets the save as CSV command.
        /// </summary>
        /// <value>
        ///     The save as CSV command.
        /// </value>
        public RelayCommand SaveAsCsvCommand { get; set; }

        /// <summary>
        ///     Gets or sets the add data command.
        /// </summary>
        /// <value>
        ///     The add data command.
        /// </value>
        public RelayCommand AddDataCommand { get; set; }

        /// <summary>
        ///     Gets or sets the load file command.
        /// </summary>
        /// <value>
        ///     The load file command.
        /// </value>
        public RelayCommand LoadFileCommand { get; set; }

        /// <summary>
        ///     Gets or sets the update data command.
        /// </summary>
        /// <value>
        ///     The update data command.
        /// </value>
        public RelayCommand UpdateDataCommand { get; set; }

        /// <summary>
        ///     Gets or sets the summary.
        /// </summary>
        /// <value>
        ///     The summary.
        /// </value>
        public static string Summary { get; set; }

        /// <summary>
        ///     Gets or sets the library.
        /// </summary>
        /// <value>
        ///     The library.
        /// </value>
        public static FitbitDataLibrary Library { get; set; }

        /// <summary>
        ///     Gets or sets the fitbit datas.
        /// </summary>
        /// <value>
        ///     The fitbit datas.
        /// </value>
        public ObservableCollection<FitbitData> FitbitDatas
        {
            get => this.fitbitDatas;
            set
            {
                this.fitbitDatas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the years.
        /// </summary>
        /// <value>
        ///     The years.
        /// </value>
        public ObservableCollection<int> Years
        {
            get => this.years;
            set
            {
                this.years = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the selected year.
        /// </summary>
        /// <value>
        ///     The selected year.
        /// </value>
        public int SelectedYear
        {
            get => this.selectedYear;
            set
            {
                this.selectedYear = value;
                this.OnPropertyChanged();
                Summary = this.reportGenerator.DisplayFitbitDataReport();
                this.FitbitDatas = this.dataLibrary.Where(data => data.Date.Year == this.SelectedYear).ToList()
                                       .ToObservableCollection();
            }
        }

        /// <summary>
        ///     Gets or sets the selected fitbit data.
        /// </summary>
        /// <value>
        ///     The selected fitbit data.
        /// </value>
        public FitbitData SelectedFitbitData
        {
            get => this.selectedFitbitData;
            set
            {
                this.selectedFitbitData = value;
                this.OnPropertyChanged();
                this.UpdateDataCommand.OnCanExecuteChanged();
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="FitbitDataDetailViewModel" /> class.
        /// </summary>
        public FitbitDataDetailViewModel()
        {
            this.dataLibrary = new FitbitDataLibrary();
            this.loadData();
            this.reportGenerator = new FitbitDataReportGenerator(this.FitbitDatas.ToFitbitDataLibrary());
            Summary = this.reportGenerator.DisplayFitbitDataReport();
            this.loadCommands();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void loadData()
        {
            this.FitbitDatas = this.dataLibrary.DataList.ToObservableCollection();
            this.Years = this.dataLibrary.GetAllYears().ToObservableCollection();
            Library = this.FitbitDatas.ToFitbitDataLibrary();
        }

        private void loadCommands()
        {
            this.LoadFileCommand = new RelayCommand(this.loadFile, this.canLoadFile);
            this.UpdateDataCommand = new RelayCommand(this.updateData, this.canUpdateData);
            this.AddDataCommand = new RelayCommand(this.addData, this.canAddData);
            this.SaveAsCsvCommand = new RelayCommand(this.saveAsCsv, this.canSaveAsCsv);
            this.SaveAsXmlCommand = new RelayCommand(this.saveAsXml, this.canSaveAsXml);
        }

        private bool canAddData(object obj)
        {
            return this.dataLibrary != null;
        }

        private async void addData(object obj)
        {
            var dialog = new AddFitbitDialog();
            await dialog.ShowAsync();

            switch (dialog.Click)
            {
                case AddFitbitDialog.Result.Add:
                    var data = new FitbitData {
                        Date = dialog.Date,
                        CaloriesBurned = dialog.CaloriesBurned,
                        Steps = dialog.Steps,
                        Distance = dialog.Distance,
                        Floors = dialog.Floors,
                        ActivityCalories = dialog.ActivityCalories,
                        ActiveMinutes = dialog.ActivityMinutes
                    };

                    this.addFitbitDataToCollection(data);
                    break;

                case AddFitbitDialog.Result.Cancel:
                    break;
            }
        }

        private void addFitbitDataToCollection(FitbitData data)
        {
            if (this.dataLibrary.Any())
            {
                this.dataLibrary.Add(data);
                this.loadData();
            }
            else
            {
                this.dataLibrary = new FitbitDataLibrary {data};
                this.loadData();
            }
        }

        private bool canSaveAsXml(object obj)
        {
            return this.FitbitDatas != null;
        }

        private async void saveAsXml(object obj)
        {
            var builder = new StringBuilder();
            var savePicker = new FileSavePicker {
                SuggestedStartLocation = PickerLocationId.DocumentsLibrary
            };
            savePicker.FileTypeChoices.Add("XML", new List<string> {".xml"});
            savePicker.SuggestedFileName = "New Document";
            var file = await savePicker.PickSaveFileAsync();
            var serializer = new XmlSerializer<FitbitData>();
            builder.Append(serializer.Serialize(this.FitbitDatas.ToFitbitDataLibrary()));
            await FileIO.WriteTextAsync(file, builder.ToString());
        }

        private bool canSaveAsCsv(object obj)
        {
            return this.dataLibrary != null;
        }

        private async void saveAsCsv(object obj)
        {
            var builder = new StringBuilder();
            var savePicker = new FileSavePicker {
                SuggestedStartLocation = PickerLocationId.DocumentsLibrary
            };
            savePicker.FileTypeChoices.Add("CSV", new List<string> {".csv"});
            savePicker.SuggestedFileName = "New Document";
            var file = await savePicker.PickSaveFileAsync();
            builder.AppendLine("Date,Calories Burned,Steps,Distance,Floors,Activity Calories,Activity Minutes");
            var allData = this.reportGenerator.GetFitbitDataInReport(this.FitbitDatas.ToFitbitDataLibrary());
            await FileIO.WriteTextAsync(file, allData);
        }

        private bool canUpdateData(object obj)
        {
            return this.SelectedFitbitData != null;
        }

        private void updateData(object obj)
        {
            var data = this.SelectedFitbitData;
            var index = this.FitbitDatas.IndexOf(this.SelectedFitbitData);
            this.FitbitDatas[index] = data;
            this.loadData();
        }

        private bool canLoadFile(object obj)
        {
            return this.FitbitDatas != null;
        }

        /// <summary>
        ///     Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void loadFile(object obj)
        {
            var file = await openFilePickerAndSelectFile();

            if (file == null)
            {
                return;
            }

            if (this.dataLibrary.Count == 0)
            {
                var processor = new DataProcessor(this.dataLibrary);
                await processor.ProcessDataWhenEmpty(file);
                this.loadData();
            }
            else
            {
                var processor = new DataProcessor(this.dataLibrary);
                await this.displayUserPromptDialog(processor, file);
            }
        }

        private static async Task<StorageFile> openFilePickerAndSelectFile()
        {
            var file = await new FileOpenPicker {
                ViewMode = PickerViewMode.Thumbnail,
                SuggestedStartLocation = PickerLocationId.DocumentsLibrary,
                FileTypeFilter = {".csv", ".txt", ".xml"}
            }.PickSingleFileAsync();
            return file;
        }

        private async Task displayUserPromptDialog(DataProcessor dataProcessor, StorageFile file)
        {
            var userpromptDialog = new ContentDialog {
                Title = "Merge or Replace",
                Content = "Would you like to merge new file into existing data or replace exisiting data?",
                PrimaryButtonText = "Merge",
                SecondaryButtonText = "Replace"
            };

            var promptResult = await userpromptDialog.ShowAsync();
            switch (promptResult)
            {
                case ContentDialogResult.Primary:
                    await dataProcessor.Merge(file);
                    this.loadData();
                    break;
                case ContentDialogResult.Secondary:
                    this.dataLibrary.Clear();
                    await dataProcessor.ProcessDataWhenEmpty(file);
                    this.loadData();
                    break;
            }
        }

        #endregion
    }
}