﻿using System.IO;
using System.Xml.Serialization;
using FitbitAnalysis.Model;

namespace FitbitAnalysis.Utility
{
    /// <summary>
    ///     Provides xml serialization services.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class XmlSerializer<T>
    {
        #region Methods

        /// <summary>
        ///     Serializes the data.
        /// </summary>
        /// <param name="library"></param>
        /// <returns></returns>
        public string Serialize(FitbitDataLibrary library)
        {
            var fitbitdataSerializer = new XmlSerializer(typeof(FitbitData));
            using (var writer = new StringWriter())
            {
                foreach (var data in library)
                {
                    fitbitdataSerializer.Serialize(writer, data);
                }

                return writer.ToString();
            }
        }

        #endregion
    }

    
}