﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using FitbitAnalysis.Model;

namespace FitbitAnalysis.Extensions
{
    /// <summary>
    ///     Provides extensions for the ObservableCollection
    /// </summary>
    public static class ListExtensions
    {
        #region Methods

        /// <summary>
        ///     To the observable collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> collection)
        {
            return new ObservableCollection<T>(collection);
        }

        /// <summary>
        ///     To the fitbit data library.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static FitbitDataLibrary ToFitbitDataLibrary(this IEnumerable<FitbitData> collection)
        {
            var library = new FitbitDataLibrary();
            library.DataList.AddRange(collection);
            return library;
        }

        #endregion
    }
}