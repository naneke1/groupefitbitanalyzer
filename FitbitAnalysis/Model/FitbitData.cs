﻿using System;

namespace FitbitAnalysis.Model
{
    /// <summary>
    ///     Models a fitbit analysis object.
    /// </summary>
    public class FitbitData
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the date.
        /// </summary>
        /// <value>
        ///     The date.
        /// </value>
        public DateTime Date { get; set; }

        /// <summary>
        ///     Gets or sets the calories burned.
        /// </summary>
        /// <value>
        ///     The calories burned.
        /// </value>
        public int CaloriesBurned { get; set; }

        /// <summary>
        ///     Gets or sets the steps.
        /// </summary>
        /// <value>
        ///     The steps.
        /// </value>
        public int Steps { get; set; }

        /// <summary>
        ///     Gets or sets the distance.
        /// </summary>
        /// <value>
        ///     The distance.
        /// </value>
        public double Distance { get; set; }

        /// <summary>
        ///     Gets or sets the floors.
        /// </summary>
        /// <value>
        ///     The floors.
        /// </value>
        public int Floors { get; set; }

        /// <summary>
        ///     Gets or sets the activity calories.
        /// </summary>
        /// <value>
        ///     The activity calories.
        /// </value>
        public int ActivityCalories { get; set; }

        /// <summary>
        ///     Gets or sets the active minutes.
        /// </summary>
        /// <value>
        ///     The active minutes.
        /// </value>
        public int ActiveMinutes { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="FitbitData" /> class.
        /// </summary>
        public FitbitData()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FitbitData" /> class.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="caloriesBurned">The calories burned.</param>
        /// <param name="steps">The steps.</param>
        /// <param name="distance">The distance.</param>
        /// <param name="floors">The floors.</param>
        /// <param name="activityCalories">The activity calories.</param>
        /// <param name="activityMinutes">The activity Minutes</param>
        public FitbitData(DateTime date, int caloriesBurned, int steps, double distance, int floors,
            int activityCalories, int activityMinutes)
        {
            this.Date = date;
            this.CaloriesBurned = caloriesBurned;
            this.Steps = steps;
            this.Distance = distance;
            this.Floors = floors;
            this.ActivityCalories = activityCalories;
            this.ActiveMinutes = activityMinutes;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Calculates the possible calories burned.
        /// </summary>
        /// <param name="steps">The steps.</param>
        /// <param name="activityCalories">The activity calories.</param>
        /// <returns></returns>
        public int CalculatePossibleCaloriesBurned(int steps, int activityCalories)
        {
            if (steps < 0 || activityCalories < 0)
            {
                throw new ArgumentException("Input values should not be less than 0");
            }

            this.Steps = steps;
            this.ActivityCalories = activityCalories;
            return steps * activityCalories;
        }

        /// <summary>
        ///     Calculates the possible distance.
        /// </summary>
        /// <param name="steps">The steps.</param>
        /// <param name="floor">The floor.</param>
        /// <returns></returns>
        public double CalculatePossibleDistance(int steps, int floor)
        {
            if (steps < 0 || floor < 0)
            {
                throw new ArgumentException("Input values should not be less than 0");
            }

            this.Steps = steps;
            this.Floors = floor;
            return steps * floor;
        }

        #endregion
    }
}