﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace FitbitAnalysis.Model
{
    /// <summary>
    ///     A class representing a collection of all fitbit analysis objects.
    /// </summary>
    public class FitbitDataLibrary : IList<FitbitData>
    {
        #region Properties

        /// <summary>
        ///     Gets the datata list.
        /// </summary>
        /// <value>
        ///     The datata list.
        /// </value>
        public List<FitbitData> DataList { get; set; }

        /// <summary>
        ///     Gets the count.
        /// </summary>
        /// <value>
        ///     The count.
        /// </value>
        public int Count => this.DataList.Count;

        /// <summary>
        ///     Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public bool IsReadOnly => false;

        /// <summary>
        ///     Gets or sets the <see cref="FitbitData" /> at the specified index.
        /// </summary>
        /// <value>
        ///     The <see cref="FitbitData" />.
        /// </value>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public FitbitData this[int index]
        {
            get => this.DataList[index];
            set => this.DataList[index] = value;
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="FitbitDataLibrary" /> class.
        /// </summary>
        public FitbitDataLibrary()
        {
            this.DataList = new List<FitbitData>();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
        ///     <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">
        ///     The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied
        ///     from <see cref="T:System.Collections.Generic.ICollection`1" />. The <see cref="T:System.Array" /> must have
        ///     zero-based indexing.
        /// </param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins.</param>
        public void CopyTo(FitbitData[] array, int arrayIndex)
        {
            this.DataList.CopyTo(array, arrayIndex);
        }

        /// <summary>
        ///     Removes the first occurrence of a specific object from the
        ///     <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        ///     true if <paramref name="item" /> was successfully removed from the
        ///     <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if
        ///     <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool Remove(FitbitData item)
        {
            return this.DataList.Remove(item);
        }

        /// <summary>
        ///     Adds the specified fitbit.
        /// </summary>
        /// <param name="fitbit">The fitbit.</param>
        public void Add(FitbitData fitbit)
        {
            if (fitbit == null)
            {
                throw new ArgumentException("Fitbit object cannot be null");
            }

            this.DataList.Add(fitbit);
        }

        /// <summary>
        ///     Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            this.DataList.Clear();
        }

        /// <summary>
        ///     Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        ///     true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />;
        ///     otherwise, false.
        /// </returns>
        public bool Contains(FitbitData item)
        {
            return this.DataList.Contains(item);
        }

        /// <summary>
        ///     Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        ///     An enumerator that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<FitbitData> GetEnumerator()
        {
            return this.DataList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) this.DataList).GetEnumerator();
        }

        /// <summary>
        ///     Determines the index of a specific item in the <see cref="T:System.Collections.Generic.IList`1" />.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.IList`1" />.</param>
        /// <returns>
        ///     The index of <paramref name="item" /> if found in the list; otherwise, -1.
        /// </returns>
        public int IndexOf(FitbitData item)
        {
            return this.DataList.IndexOf(item);
        }

        /// <summary>
        ///     Inserts an item to the <see cref="T:System.Collections.Generic.IList`1" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="item" /> should be inserted.</param>
        /// <param name="item">The object to insert into the <see cref="T:System.Collections.Generic.IList`1" />.</param>
        public void Insert(int index, FitbitData item)
        {
            this.DataList.Insert(index, item);
        }

        /// <summary>
        ///     Removes the <see cref="T:System.Collections.Generic.IList`1" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            this.DataList.RemoveAt(index);
        }

        /// <summary>
        ///     Counts the days in year with no data.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <returns></returns>
        public object CountDaysInYearWithNoData(int year)
        {
            int datacount;

            if (DateTime.IsLeapYear(year))
            {
                datacount = DaysInLeapYear - this.DataList.Count(fitbit => fitbit.Date.Year == year);
            }
            else
            {
                datacount = DaysInYear - this.DataList.Count(fitbit => fitbit.Date.Year == year);
            }
            return datacount;
        }

        /// <summary>
        ///     Gets all years.
        /// </summary>
        /// <returns></returns>
        public IList<int> GetAllYears()
        {
            var years = new List<int>();
            foreach (var data in this.DataList)
            {
                if (!years.Contains(data.Date.Year))
                {
                    years.Add(data.Date.Year);
                }
            }
            return years;
        }

        /// <summary>
        ///     Calculates the average steps.
        /// </summary>
        /// <returns></returns>
        public double CalculateAverageSteps()
        {
            return this.DataList.Count > 0 ? this.DataList.Average(data => data.Steps) : 0;
        }

        /// <summary>
        ///     Retrieves all data with steps between.
        /// </summary>
        /// <param name="firstvalue">The firstvalue.</param>
        /// <param name="secondvalue">The secondvalue.</param>
        /// <returns></returns>
        public int CountAllDataWithStepsBetween(int firstvalue, int secondvalue)
        {
            if (firstvalue < 0 || secondvalue < 0)
            {
                throw new ArgumentException("Input values should not be less than 0");
            }

            if (secondvalue < firstvalue)
            {
                throw new ArgumentException("Second value should not be less than first value");
            }

            var steps = this.DataList.Count(data => data.Steps > firstvalue && data.Steps < secondvalue);
            return steps;
        }

        /// <summary>
        ///     Retrieves all data with steps greater than.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public int CountAllDataWithStepsGreaterThan(int value)
        {
            if (value < 0)
            {
                throw new ArgumentException("Input values should not be less than 0");
            }

            var steps = this.DataList.Count(data => data.Steps > value);
            return steps;
        }

        /// <summary>
        ///     Counts the number of days in month.
        /// </summary>
        /// <param name="month">The month.</param>
        /// <returns></returns>
        public int CountNumberOfDataInMonth(int month)
        {
            if (month < 1 || month > 12)
            {
                throw new ArgumentException("Value entered is not a month.");
            }

            var days = this.DataList.Count(data => data.Date.Month.Equals(month));
            return days;
        }

        /// <summary>
        ///     Counts the number of months with no data.
        /// </summary>
        /// <returns></returns>
        public int CountNumberOfMonthsWithNoData()
        {
            var months = this.DataList.GroupBy(data => data.Date.Month);
            var keys = new List<int>();
            var missingmonths = new List<int>();
            var count = 0;

            foreach (var month in months)
            {
                keys.Add(month.Key);
            }

            for (var i = 1; i <= 12; i++)
            {
                missingmonths.Add(i);
            }

            foreach (var month in missingmonths)
            {
                if (!keys.Contains(month))
                {
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        ///     Retrieves the highest activity minutes all year.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <returns></returns>
        public FitbitData RetrieveHighestActivityMinutesAllYear(int year)
        {
            if (year < 0)
            {
                throw new ArgumentException("Year entered is not a  valid input.");
            }
            if (this.DataList.Any(fitbit => fitbit.Date.Year == year))
            {
                var highestActivityMinutes = this.DataList.Where(y => y.Date.Year == year).Max(x => x.ActiveMinutes);
                var fitBit = this.DataList.Find(singleFitBit => singleFitBit.ActiveMinutes == highestActivityMinutes);
                return fitBit;
            }
            throw new ArgumentException("Year entered is not in list.");
        }

        /// <summary>
        ///     Retrieves the highest activity minutes all month.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month">The month.</param>
        /// <returns></returns>
        public double RetrieveAverageActivityMinutesAInMonth(int year, int month)
        {
            double average;
            if (month < 1 || month > 12)
            {
                throw new ArgumentException("Month must be between 1 and 12.");
            }
            if (year < 0)
            {
                throw new ArgumentException("Year can not be negative.");
            }
            if (this.DataList.Any(x => x.Date.Month == month && x.Date.Year == year))
            {
                average = this.DataList.Count > 0
                    ? this.DataList.Where(fitBit => fitBit.Date.Year == year && fitBit.Date.Month == month)
                          .Average(data => data.ActiveMinutes)
                    : 0;
            }
            else
            {
                average = 0.0;
            }
            return average;
        }

        /// <summary>
        ///     Countdayses the in month.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <returns></returns>
        public int CountdaysInMonth(int year, int month)
        {
            var count = this.DataList.Count(x => x.Date.Year == year && x.Date.Month == month);
            return count;
        }

        /// <summary>
        ///     Gets the steps maximum for month.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <returns></returns>
        public int RetrieveStepsMaxForMonth(int year, int month)
        {
            int maxSteps;
            if (month < 1 || month > 12)
            {
                throw new ArgumentException("Month must be between 1 and 12.");
            }
            if (year < 0)
            {
                throw new ArgumentException("Year can not be negative.");
            }
            if (this.DataList.Any(fitbit => fitbit.Date.Year == year && fitbit.Date.Month == month))
            {
                maxSteps = this.DataList.Where(fitBit => fitBit.Date.Year == year && fitBit.Date.Month == month)
                               .Max(fitbitData => fitbitData.Steps);
            }
            else
            {
                maxSteps = 0;
            }
            return maxSteps;
        }

        /// <summary>
        ///     Retrieves the minimum steps for month.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <returns></returns>
        public object RetrieveMinStepsForMonth(int year, int month)
        {
            int minSteps;
            if (month < 1 || month > 12)
            {
                throw new ArgumentException("Month must be between 1 and 12.");
            }
            if (year < 0)
            {
                throw new ArgumentException("Year can not be negative.");
            }
            if (this.DataList.Any(fitbit => fitbit.Date.Year == year && fitbit.Date.Month == month))
            {
                minSteps = this.DataList.Where(fitBit => fitBit.Date.Year == year && fitBit.Date.Month == month)
                               .Min(fitbitData => fitbitData.Steps);
            }
            else
            {
                minSteps = 0;
            }
            return minSteps;
        }

        /// <summary>
        ///     Calculates the average for month.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <returns></returns>
        public double CalculateAverageForMonth(int year, int month)
        {
            double avgSteps;
            if (month < 1 || month > 12)
            {
                throw new ArgumentException("Month must be between 1 and 12.");
            }
            if (year < 0)
            {
                throw new ArgumentException("Year can not be negative.");
            }
            if (this.DataList.Any(x => x.Date.Month == month && x.Date.Year == year))
            {
                avgSteps = this.DataList.Where(fitBit => fitBit.Date.Year == year && fitBit.Date.Month == month)
                               .Average(fitbitData => fitbitData.Steps);
            }
            else
            {
                avgSteps = 0.0;
            }
            return avgSteps;
        }

        #endregion

        #region Data Members

        private const int DaysInYear = 365;
        private const int DaysInLeapYear = 366;

        #endregion
    }
}