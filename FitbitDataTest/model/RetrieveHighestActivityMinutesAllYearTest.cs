﻿using System;
using FitbitAnalysis.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FitbitDataTest.model
{
    /**
     * TestSunnyDayRetrieveActivity
     * TestRainyDayRetrieveActivityWhenParameterIsNegative
     * TestRainyDayRetrieveActivityWhenYearIsNotInList
     * TestIfAllDaysHaveTheSameAtiveMinutes
     * TestIfDiffrentYearsArePresent
     */
    [TestClass]
    public class RetrieveHighestActivityMinutesAllYearTest
    {

        [TestMethod]
        public void TestSunnyDayRetrieveActivity()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2016, 2, 1), 1710, 7006, 3.18, 30, 558, 190),
                new FitbitData(new DateTime(2016, 8, 2), 1600, 5478, 2.53, 30, 446, 90),
                new FitbitData(new DateTime(2016, 9, 3), 954, 5756, 2.48, 30, 540, 67),
                new FitbitData(new DateTime(2016, 11, 11), 340, 3756, 1.76, 30, 120, 8)
            };

            var result = dataLibrary.RetrieveHighestActivityMinutesAllYear(2016);

            Assert.AreEqual(190, result.ActiveMinutes);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Year entered is not a  valid input.")]
        public void TestRainyDayRetrieveActivityWhenParameterIsNegative()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2016, 2, 1), 1710, 7006, 3.18, 30, 558, 190),
                new FitbitData(new DateTime(2016, 8, 2), 1600, 5478, 2.53, 30, 446, 90),
                new FitbitData(new DateTime(2016, 9, 3), 954, 5756, 2.48, 30, 540, 67),
                new FitbitData(new DateTime(2016, 11, 11), 340, 3756, 1.76, 30, 120, 8)
            };

            dataLibrary.RetrieveHighestActivityMinutesAllYear(-2016);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Year entered is not in list.")]
        public void TestRainyDayRetrieveActivityWhenYearIsNotInList()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2016, 2, 1), 1710, 7006, 3.18, 30, 558, 190),
                new FitbitData(new DateTime(2016, 8, 2), 1600, 5478, 2.53, 30, 446, 90),
                new FitbitData(new DateTime(2016, 9, 3), 954, 5756, 2.48, 30, 540, 67),
                new FitbitData(new DateTime(2016, 11, 11), 340, 3756, 1.76, 30, 120, 8)
            };

            dataLibrary.RetrieveHighestActivityMinutesAllYear(2015);
        }

        [TestMethod]
        public void TestIfAllDaysHaveTheSameAtiveMinutes()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102),
                new FitbitData(new DateTime(2016, 2, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 8, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 9, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 11, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            var result = dataLibrary.RetrieveHighestActivityMinutesAllYear(2016);

            Assert.AreEqual(102, result.ActiveMinutes);
        }

        [TestMethod]
        public void TestIfDiffrentYearsArePresent()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2015, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2014, 2, 1), 1710, 7006, 3.18, 30, 558, 190),
                new FitbitData(new DateTime(2016, 8, 2), 1600, 5478, 2.53, 30, 446, 90),
                new FitbitData(new DateTime(2016, 9, 3), 954, 5756, 2.48, 30, 540, 67),
                new FitbitData(new DateTime(2016, 11, 11), 340, 3756, 1.76, 30, 120, 8)
            };

            var result = dataLibrary.RetrieveHighestActivityMinutesAllYear(2016);

            Assert.AreEqual(102, result.ActiveMinutes);
        }
    }
}
