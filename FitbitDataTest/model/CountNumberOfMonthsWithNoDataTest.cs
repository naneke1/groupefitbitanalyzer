﻿using System;
using FitbitAnalysis.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FitbitDataTest.model
{
    [TestClass]
    public class CountNumberOfMonthsWithNoDataTest
    {
        /*
         *CountNumberOfMonthsWithNoData
         * CountNumberOfMonthsWithNoDataWhenListIsEmpty
         * CountNumberOfMonthsWithNoDataWhen11MonthsPresent
         * CountNumberOfMonthsWithNoDataWhen1MonthsPresent
         * CountNumberOfMonthsWithWhenAllMonthsPresent
         */
        [TestMethod]
        public void CountNumberOfMonthsWithNoData()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            var result = dataLibrary.CountNumberOfMonthsWithNoData();

            Assert.AreEqual(9, result);
        }


        [TestMethod]
        public void CountNumberOfMonthsWithNoDataWhenListIsEmpty()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary ();

            var result = dataLibrary.CountNumberOfMonthsWithNoData();

            Assert.AreEqual(12, result);
        }


        [TestMethod]
        public void CountNumberOfMonthsWithNoDataWhen11MonthsPresent()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 2, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 3, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 4, 11), 340, 3756, 1.76, 30, 120, 102),
                new FitbitData(new DateTime(2016, 5, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 6, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 8, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 9, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 10, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 11, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 3, 3), 954, 5756, 2.48, 30, 540, 102),
            };

            var result = dataLibrary.CountNumberOfMonthsWithNoData();

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void CountNumberOfMonthsWithNoDataWhen1MonthsPresent()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                
            };

            var result = dataLibrary.CountNumberOfMonthsWithNoData();

            Assert.AreEqual(11, result);
        }


        [TestMethod]
        public void CountNumberOfMonthsWithWhenAllMonthsPresent()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 12, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 2, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 3, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 4, 11), 340, 3756, 1.76, 30, 120, 102),
                new FitbitData(new DateTime(2016, 5, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 6, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 8, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 9, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 10, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 11, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 3, 3), 954, 5756, 2.48, 30, 540, 102),

            };

            var result = dataLibrary.CountNumberOfMonthsWithNoData();

            Assert.AreEqual(0, result);
        }
    }
}
