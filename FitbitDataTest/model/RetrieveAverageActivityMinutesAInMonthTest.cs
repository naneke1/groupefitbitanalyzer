﻿using System;
using FitbitAnalysis.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FitbitDataTest.model
{
    /**
     * RetrieveAverageActivityMinutesAInMonthTest
     * TestSunnyDayIfListHasMonthWithOneInput
     * TestSunnyDayIfListHasMonthButInDiffrentYear
     * TestSunnyDayIfListHasMonthWithMultipleDays
     * TestRainyDayIfListDoesntContainMonth
     * TestRainyDayIfListIsEmpty
     * TestRainyDayRetrieveMaxStepsInMonthWhenMonthIsOutOfRange
     * TestRainyDayRetrieveMaxStepsInMonthWhenYearIsOutOfRange
     */
    [TestClass]
    public class RetrieveAverageActivityMinutesAInMonthTest
    {

        [TestMethod]
        public void TestSunnyDayIfListHasMonthWithOneDateZeroActiveMinutes()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 12, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 12, 2), 1600, 5432, 2.53, 30, 446, 0),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10)
            };

            var result = dataLibrary.RetrieveAverageActivityMinutesAInMonth(2016, 12);
            Assert.AreEqual(37.33, Math.Round(result, 2));
        }
        [TestMethod]
        public void TestSunnyDayIfListHasMonthWithOneInput()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 12, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10)
            };

            var result = dataLibrary.RetrieveAverageActivityMinutesAInMonth(2016, 1);
            Assert.AreEqual(12, result);
        }

        [TestMethod]
        public void TestSunnyDayIfListHasMonthButInDiffrentYear()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 12, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2017, 12, 11), 340, 3756, 1.76, 30, 120, 10)
            };

            var result = dataLibrary.RetrieveAverageActivityMinutesAInMonth(2016, 12);
            Assert.AreEqual(102, result);
        }

        [TestMethod]
        public void TestSunnyDayIfListHasMonthWithMultipleDays()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 12, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10)
            };

            var result = dataLibrary.RetrieveAverageActivityMinutesAInMonth(2016, 12);
            Assert.AreEqual(56, result);
        }

        [TestMethod]
        public void TestRainyDayIfListDoesntContainMonth()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 12, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10)
            };

            var result = dataLibrary.RetrieveAverageActivityMinutesAInMonth(2016, 2);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void TestRainyDayIfListIsEmpty()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary ();

            var result = dataLibrary.RetrieveAverageActivityMinutesAInMonth(2017, 12);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Month must be between 1 and 12.")]
        public void TestRainyDayRetrieveMaxStepsInMonthWhenMonthIsOutOfRange()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 12, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2016, 2, 1), 1710, 2345, 3.18, 30, 558, 190),
                new FitbitData(new DateTime(2016, 8, 2), 1600, 5634, 2.53, 30, 446, 90),
                new FitbitData(new DateTime(2016, 9, 3), 954, 2341, 2.48, 30, 540, 67),
                new FitbitData(new DateTime(2016, 11, 11), 340, 7002, 1.76, 30, 120, 8)
            };

            dataLibrary.RetrieveAverageActivityMinutesAInMonth(2016, 13);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Year can not be negative.")]
        public void TestRainyDayRetrieveMaxStepsInMonthWhenYearIsOutOfRange()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 12, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2016, 2, 1), 1710, 2345, 3.18, 30, 558, 190),
                new FitbitData(new DateTime(2016, 8, 2), 1600, 5634, 2.53, 30, 446, 90),
                new FitbitData(new DateTime(2016, 9, 3), 954, 2341, 2.48, 30, 540, 67),
                new FitbitData(new DateTime(2016, 11, 11), 340, 7002, 1.76, 30, 120, 8)
            };

            dataLibrary.RetrieveAverageActivityMinutesAInMonth(-2017, 1);
        }
    }
}
