﻿using System;
using FitbitAnalysis.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FitbitDataTest.model
{
    [TestClass]
    public class RetrieveMinStepsForMonthTest
    {
        /**
         * TestSunnyDayRetrieveMinStepsInMonthIfSameYearDiffrentMonths
         * TestSunnyDayRetrieveMinStepsInMonthIfDiffrentYearSameMonths
         * TestSunnyDayRetrieveMinStepsInMonthIfDiffrentYearDiffrentMonths
         * TestSunnyDayRetrieveMinStepsInMonthWhenMonthHasNoData
         * TestSunnyDayRetrieveMinStepsInMonthWhenMonthHasNoDataInCurrentYear
         * TestRainyDayRetrieveMinStepsInMonthWhenMonthIsOutOfRange
         * TestRainyDayRetrieveMinStepsInMonthWhenYearIsOutOfRange
         */

        [TestMethod]
        public void TestSunnyDayRetrieveMinStepsInMonthIfSameYearDiffrentMonths()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2016, 2, 1), 1710, 2345, 3.18, 30, 558, 190),
                new FitbitData(new DateTime(2016, 8, 2), 1600, 5634, 2.53, 30, 446, 90),
                new FitbitData(new DateTime(2016, 9, 3), 954, 2341, 2.48, 30, 540, 67),
                new FitbitData(new DateTime(2016, 11, 11), 340, 7002, 1.76, 30, 120, 8)
            };

            var result = dataLibrary.RetrieveMinStepsForMonth(2016, 1);

            Assert.AreEqual(5478, result);
        }


        [TestMethod]
        public void TestSunnyDayRetrieveMinStepsInMonthIfDiffrentYearSameMonths()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2015, 1, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 1, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2018, 2, 1), 1710, 2345, 3.18, 30, 558, 190)
            };

            var result = dataLibrary.RetrieveMinStepsForMonth(2016, 1);

            Assert.AreEqual(3756, result);
        }

        [TestMethod]
        public void TestSunnyDayRetrieveMinStepsInMonthIfDiffrentYearDiffrentMonths()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2016, 2, 1), 1710, 2345, 3.18, 30, 558, 190)
            };

            var result = dataLibrary.RetrieveMinStepsForMonth(2017, 11);

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void TestSunnyDayRetrieveMinStepsInMonthWhenMonthHasNoData()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2016, 2, 1), 1710, 2345, 3.18, 30, 558, 190),
                new FitbitData(new DateTime(2016, 8, 2), 1600, 5634, 2.53, 30, 446, 90),
                new FitbitData(new DateTime(2016, 9, 3), 954, 2341, 2.48, 30, 540, 67),
                new FitbitData(new DateTime(2016, 11, 11), 340, 7002, 1.76, 30, 120, 8)
            };

            var result = dataLibrary.RetrieveMinStepsForMonth(2016, 3);

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void TestSunnyDayRetrieveMinStepsInMonthWhenMonthHasNoDataInCurrentYear()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 12, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2016, 2, 1), 1710, 2345, 3.18, 30, 558, 190),
                new FitbitData(new DateTime(2016, 8, 2), 1600, 5634, 2.53, 30, 446, 90),
                new FitbitData(new DateTime(2016, 9, 3), 954, 2341, 2.48, 30, 540, 67),
                new FitbitData(new DateTime(2016, 11, 11), 340, 7002, 1.76, 30, 120, 8)
            };

            var result = dataLibrary.RetrieveMinStepsForMonth(2017, 12);

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Month must be between 1 and 12.")]
        public void TestRainyDayRetrieveMinStepsInMonthWhenMonthIsOutOfRange()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 12, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2016, 2, 1), 1710, 2345, 3.18, 30, 558, 190),
                new FitbitData(new DateTime(2016, 8, 2), 1600, 5634, 2.53, 30, 446, 90),
                new FitbitData(new DateTime(2016, 9, 3), 954, 2341, 2.48, 30, 540, 67),
                new FitbitData(new DateTime(2016, 11, 11), 340, 7002, 1.76, 30, 120, 8)
            };

            dataLibrary.RetrieveMinStepsForMonth(2017, 13);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Year can not be negative.")]
        public void TestRainyDayRetrieveMinStepsInMonthWhenYearIsOutOfRange()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 12, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10),
                new FitbitData(new DateTime(2016, 2, 1), 1710, 2345, 3.18, 30, 558, 190),
                new FitbitData(new DateTime(2016, 8, 2), 1600, 5634, 2.53, 30, 446, 90),
                new FitbitData(new DateTime(2016, 9, 3), 954, 2341, 2.48, 30, 540, 67),
                new FitbitData(new DateTime(2016, 11, 11), 340, 7002, 1.76, 30, 120, 8)
            };

            dataLibrary.RetrieveMinStepsForMonth(-2017, 1);
        }
    }
}
