﻿using System;
using FitbitAnalysis.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FitbitDataTest.model
{
    [TestClass]
    public class CalculateAverageTest
    {
        /**
         * TestSunnyDayIfListHasMonthWithOneDateZeroSteps
         * TestSunnyDayCalculateAverage
         * TestEmptyCalculateAverage
         * TestWhenOneCalculateAverage
         */

        [TestMethod]
        public void TestSunnyDayIfListHasMonthWithOneDateZeroSteps()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 12, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 12, 2), 1600, 0, 2.53, 30, 446, 12),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 122),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 10)
            };

            var result = dataLibrary.CalculateAverageSteps();
            Assert.AreEqual(4129.5, Math.Round(result, 2));
        }

        [TestMethod]
        public void TestSunnyDayCalculateAverage()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            var result = dataLibrary.CalculateAverageSteps();

            Assert.AreEqual(5499, result);
        }

        [TestMethod]
        public void TestEmptyCalculateAverage()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary();

            var result = dataLibrary.CalculateAverageSteps();

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void TestWhenOneCalculateAverage()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary
            {
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            var result = dataLibrary.CalculateAverageSteps();

            Assert.AreEqual(3756, result);
        }
    }
}
