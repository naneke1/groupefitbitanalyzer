﻿using System;
using FitbitAnalysis.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FitbitDataTest.model
{
    /**
     * CountStepsBetweenFirstParameterIsNegative1
     * CountStepsBetweenSecondParameterIsNegative1
     * CountStepsBetweenFirstParameterIs0
     * CountStepsBetweenSecondParameterIsLessThanFirstParameter
     * CountStepsBetweenFirstParameterIs1
     * CountStepsBetweenTwoValues
     * CountStepsBetweenTwoValuesWhenThresholdTheSameValue
     * CountStepsBetweenTwoValuesWhenNoneMatchThresold
     */

    [TestClass]
    public class CountStepsBetween
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Input values should not be less than 0")]
        public void CountStepsBetweenFirstParameterIsNegative1()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            dataLibrary.CountAllDataWithStepsBetween(-1, 5000);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Input values should not be less than 0")]
        public void CountStepsBetweenSecondParameterIsNegative1()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            dataLibrary.CountAllDataWithStepsBetween(1000, -1);
        }

        [TestMethod]
        public void CountStepsBetweenFirstParameterIs0()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 2478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            var result = dataLibrary.CountAllDataWithStepsBetween(0, 5000);

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Second value should not be less than first value")]
        public void CountStepsBetweenSecondParameterIsLessThanFirstParameter()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };
            
            dataLibrary.CountAllDataWithStepsBetween(1000, 0);
        }

        [TestMethod]
        public void CountStepsBetweenFirstParameterIs1()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            var result = dataLibrary.CountAllDataWithStepsBetween(1, 6000);

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void CountStepsBetweenTwoValues()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 2478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };
            
            var result = dataLibrary.CountAllDataWithStepsBetween(4000, 7500);

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void CountStepsBetweenTwoValuesWhenThresholdTheSameValue()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 2478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            var result = dataLibrary.CountAllDataWithStepsBetween(7006, 7006);

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void CountStepsBetweenTwoValuesWhenNoneMatchThresold()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 2478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            var result = dataLibrary.CountAllDataWithStepsBetween(10000, 15000);

            Assert.AreEqual(0, result);
        }
    }
}
