﻿using System;
using FitbitAnalysis.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FitbitDataTest.model
{
    [TestClass]
    public class CountNumberOfDataInMonthTest
    {
        /**
         * CountNumberOfDaysInNegativeMonth
         * CountNumberOfDaysInMonthZero
         * CountNumberOfDaysInMonthOne
         * CountNumberOfDaysInMonthSeven
         * CountNumberOfDaysInMonthTwelve
         * CountNumberOfDaysInMonthThirteen
         * CountNumberOfDaysInMonthTwelveWhenNone
         * CountNumberOfDaysIfListEmpty
         */
        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Value entered is not a month.")]
        public void CountNumberOfDaysInNegativeMonth()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            dataLibrary.CountNumberOfDataInMonth(-1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Value entered is not a month.")]
        public void CountNumberOfDaysInMonthZero()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            dataLibrary.CountNumberOfDataInMonth(0);
        }

        [TestMethod]
        public void CountNumberOfDaysInMonthOne()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            var result = dataLibrary.CountNumberOfDataInMonth(1);

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void CountNumberOfDaysInMonthSeven()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            var result = dataLibrary.CountNumberOfDataInMonth(7);

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void CountNumberOfDaysInMonthTwelve()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            var result = dataLibrary.CountNumberOfDataInMonth(12);

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Value entered is not a month.")]
        public void CountNumberOfDaysInMonthThirteen()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            dataLibrary.CountNumberOfDataInMonth(13);
        }

        [TestMethod]
        public void CountNumberOfDaysInMonthTwelveWhenNone()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 1, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            var result = dataLibrary.CountNumberOfDataInMonth(12);

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void CountNumberOfDaysIfListEmpty()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary();

            var result = dataLibrary.CountNumberOfDataInMonth(12);

            Assert.AreEqual(0, result);
        }
    }
}
