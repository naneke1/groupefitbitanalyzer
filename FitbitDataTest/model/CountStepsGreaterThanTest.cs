﻿using System;
using FitbitAnalysis.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FitbitDataTest.model
{
    [TestClass]
    public class CountStepsGreaterThanTest
    {
        /**
         * CountStepsGreaterThanNegative1
         * CountStepsGreaterThan0
         * CountStepsGreaterThan1
         * CountStepsGreaterThan5000
         * CountStepsGreaterThan5000WhenNoneAre
         * 
         */

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Input values should not be less than 0")]
        public void CountStepsGreaterThanNegative1()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            dataLibrary.CountAllDataWithStepsGreaterThan(-1);
        }

        [TestMethod]
        public void CountStepsGreaterThan0()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            int result = dataLibrary.CountAllDataWithStepsGreaterThan(0);

            Assert.AreEqual(4, result);
        }

        [TestMethod]
        public void CountStepsGreaterThan1()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 5478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            int result = dataLibrary.CountAllDataWithStepsGreaterThan(1);

            Assert.AreEqual(4, result);
        }

        [TestMethod]
        public void CountStepsGreaterThan5000()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 7006, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 2478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 5756, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            int result = dataLibrary.CountAllDataWithStepsGreaterThan(5000);

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void CountStepsGreaterThan5000WhenNoneAre()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary {
                new FitbitData(new DateTime(2016, 1, 1), 1710, 706, 3.18, 30, 558, 102),
                new FitbitData(new DateTime(2016, 1, 2), 1600, 2478, 2.53, 30, 446, 102),
                new FitbitData(new DateTime(2016, 7, 3), 954, 556, 2.48, 30, 540, 102),
                new FitbitData(new DateTime(2016, 12, 11), 340, 3756, 1.76, 30, 120, 102)
            };

            int result = dataLibrary.CountAllDataWithStepsGreaterThan(5000);

            Assert.AreEqual(0, result);
        }
    }
}
